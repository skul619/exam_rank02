/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fizzbuzz.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jpark <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/04/02 15:12:57 by jpark             #+#    #+#             */
/*   Updated: 2024/04/02 15:18:21 by jpark            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_putstr(char *str)
{
	int	i;

	i = 0;
	while (str[i])
	{
		write(1, &str[i], 1);
		i++;
	}
}

int	main(void)
{
	int	n;

	n = 1;
	while (n < 101)
	{
		if (n % 3 == 0 && n % 5 == 0)
			ft_putstr("fizzbuzz");
		else if (n % 3 == 0)
			ft_putstr("fizz");
		else if (n % 5 == 0)
			ft_putstr("buzz");
		else
		{
			if (n > 9)
			{
				ft_putchar(n / 10 + 48);
				ft_putchar(n % 10 + 48);
			}
			else
				ft_putchar(n + 48);
		}
		ft_putchar('\n');
		n++;
	}
}
