/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   expand_str.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: joleal-b <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/27 16:31:32 by joleal-b          #+#    #+#             */
/*   Updated: 2024/03/27 17:35:10 by joleal-b         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int ft_isspace(char c)
{
    return (c == 32 || c == 9);
}

int main(int ac, char **av)
{
    char    *str;

    if (ac == 2)
    {
        str = av[1];
        while(ft_isspace(*str))
            ++str;
        while (*str)
        {
            while(!ft_isspace(*str) && *str)
                write(1, str++, 1);
            while (ft_isspace(*str))
            {
                ++str;
                if (!*str)
                    break;
            }
            if (!ft_isspace(*str) && *str)
                write(1, "   ", 3);                
        }
    }
    write(1, "\n", 1);
}