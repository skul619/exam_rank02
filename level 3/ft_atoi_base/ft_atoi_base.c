/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jpark <jpark@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/04/01 10:51:40 by jpark             #+#    #+#             */
/*   Updated: 2024/04/01 11:09:11 by jpark            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int	ft_atoi_base(const char *str, int str_base)
{
    int sign;
    int res;

    sign = 1;
    res = 0;
    if (str_base < 2 || str_base > 16)
        return (NULL);
    while (*str == ' ' || *str == '\t')
        str++;
    if (*str == '-' || *str == '+')
    {
        if (*str == '-')
        {
            sign = sign * -1;
        }
        str++;
    }
    while (*str)
    {
        int dig = 0; 
        
        if (*str >= '0' && *str <= '9')
        {
            dig = *str - '0';
        }
        else if (*str >= 'a' && *str <= 'f')
        {
            dig = *str - 'a' + 10;
        }
        else if (*str >= 'A' && *str <= 'F')
        {
            dig = *str - 'A' + 10;
        }
        else
            break;
        if (dig >= str_base)
            break;
        res = res * str_base + dig;
        str++;
    }
    return (res * sign);
}