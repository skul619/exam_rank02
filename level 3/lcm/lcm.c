/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lcm.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: joleal-b <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/28 08:36:09 by joleal-b          #+#    #+#             */
/*   Updated: 2024/03/28 08:38:59 by joleal-b         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int pgcd(int a, int b)
{
    if (b == 0)
        return (a);
    else
        return (pgcd(b, a %b));
}

unsigned int    lcm(unsigned int a, unsigned b)
{
    if((int) a <= 0 || (int) b <= 0)
        return (0);
    else
        return ((a * b) / pgcd(a, b));
}