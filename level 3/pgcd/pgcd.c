/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pgcd.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: joleal-b <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/04/03 11:33:26 by joleal-b          #+#    #+#             */
/*   Updated: 2024/04/03 11:59:00 by joleal-b         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>

int	pgcd(int a, int b)
{
	if (b == 0)
		return (a);
	else
		return(pgcd(b, a % b));
}

int	main(int ac, char **av)
{
	if (ac == 3)
	{
		int	n1;
		int	n2;
		int	res;

		n1 = atoi(av[1]);
		n2 = atoi(av[2]);
		if (n1 > 0 && n2 > 0)
		{
			res = pgcd(n1, n2);
			printf("%d", res);
		}
	}
	printf("\n");
}
