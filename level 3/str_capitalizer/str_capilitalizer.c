/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   str_capilitalizer.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: joleal-b <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/04/02 22:10:18 by joleal-b          #+#    #+#             */
/*   Updated: 2024/04/02 22:11:37 by joleal-b         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

char    *ft_capitalizer(char *str)
{
    int i;

    i = 0;
    while (str[i])
    {
        if (str[i] >= 'A' && str[i] <= 'Z')
        {
            str[i] = str[i] + 32;
        }
        i++;
    }
    i = 0;
    while (str[i])
    {
        if (str[0] >= 'a' && str[0] <= 'z')
        {
            str[i] = str[i] - 32;
        }
        else if ((str[i] >= 'a' && str[i] <= 'z')
            && (str[i - 1] == 32 || str[i - 1] == 9))
            {
                str[i] = str[i] - 32;
            }
        write(1, &str[i], 1);
        i++;
    }
    return (str);
}

int main(int ac, char **av)
{
    int i;

    i = 1;
    while (ac > i)
    {
        ft_capitalizer(av[i]);
        i++;
    }
    write(1, "\n", 1);
}