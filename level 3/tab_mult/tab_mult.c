/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tab_mult.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jpark <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/04/02 12:04:44 by jpark             #+#    #+#             */
/*   Updated: 2024/04/02 12:50:23 by jpark            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <limits.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_putstr(char *str)
{
	int	i;

	i = 0;
	while (str[i])
	{
		write(1, &str[i], 1);
		i++;
	}
}

int	ft_atoi(char *str)
{
	int	sign;
	int	res;
	int	i;

	sign = 1;
	res = 0;
	i = 0;

	while (str[i] == 32 || str[i] == 9)
		i++;
	if (str[i] == 43 || str[i] == 45)
	{
		if (str[i] == 45)
			sign = sign * -1;
		i++;
	}
	while (str[i] >= 48 && str[i] <= 57)
	{
		res = ((res * 10) + (str[i] - 48));
		i++;
	}
	return (sign * res);
}

void	ft_putnbr(int nbr)
{
	if (nbr == -2147483648)
	{
		write(1, "-2147483648", 11);
	}
	else if (nbr < 0)
	{
		write(1, "-", 1);
		nbr = -nbr;
		ft_putnbr(nbr);
	}
	else if (nbr > 9)
	{
		ft_putnbr(nbr / 10);
		ft_putnbr(nbr % 10);
	}
	else
		ft_putchar(nbr + 48);
}

int	main(int ac, char **av)
{
	int	i;
	long	m;
	int	res;

	i = 1;
	res = 0;
	if (ac == 2)
	{
		m = ft_atoi(av[1]);
		if (m >= 0 && (m * 9 <= INT_MAX))
		{
			while (i < 10)
			{
				ft_putnbr(i);
				write(1, " x ", 3);
				ft_putnbr(m);
				write(1, " = ", 3);
				res = i * m;
				ft_putnbr(res);
				write(1, "\n", 1);
				i++;
			}
		}
	}
	else
		write(1, "\n", 1);
}
