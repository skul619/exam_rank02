/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_hex.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: joleal-b <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/20 14:59:26 by joleal-b          #+#    #+#             */
/*   Updated: 2024/03/20 16:28:39 by joleal-b         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void    ft_putchar(char c)
{
    write(1, &c, 1);
}

int ft_atoi(char *str)
{
    int i;
    int sign;
    int res;

    i = 0;
    sign = 1;
    res = 0;
    
    while (str[i] == 32 || str[i] > 8 && str[i] < 14)
        i++;
    while (str[i] == 45 || str[i] == 43)
    {
        if (str[i] == 43)
        {
            sign = sign * -1;
        }
        i++;
    }
    while (str[i] > 47 && str[i] < 58)
    {
        res = res * 10 + str[i] - 48;
        i++;
    }
    return (sign * res);
}

void    print_hex(int n)
{
    char    *base = "0123456789abcdef";

    if (n > 16)
        print_hex(n / 16);
    ft_putchar(base[n % 16]);
}

int main(int ac, char **av)
{
    if (ac == 2)
    {
        long n = ft_atoi(av[1]);
        if (n >= 0)
            print_hex(n);
    }
    write (1, "\n", 1);
}