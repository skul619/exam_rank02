/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   paramsum.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: joleal-b <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/27 11:39:55 by joleal-b          #+#    #+#             */
/*   Updated: 2024/03/27 12:00:38 by joleal-b         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void    ft_putchar(char c)
{
    write(1, &c, 1);
}

void    ft_putnbr(int n)
{
    if(n == -2147483648)
    {
        write(1, "-2147483648", 11);
    }
    else if(n < 0)
    {
        write(1, "-", 1);
        n = -n;
        ft_putnbr(n);
    }
    else if(n > 9)
    {
        ft_putnbr(n / 10);
        ft_putnbr(n % 10);
    }
    else if(n < 10)
    {
        ft_putchar(n + 48);
    }
}

int main(int ac, char **av)
{
    int i;

    (void)av;
    i = ac - 1;
    if (ac == 0)
    {
        write(1, "0\n", 2);
    }
    if (ac > 0)
    {
        ft_putnbr(i);
        write(1, "\n", 1);
    }
}