/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rostring.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jpark <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/04/02 15:44:29 by jpark             #+#    #+#             */
/*   Updated: 2024/04/02 16:21:56 by jpark            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int	main(int ac, char **av)
{
	int	i;
	int	start;
	int	end;
	int	flag;

	flag = 0;
	if(ac > 1)
	{
		i = 0;
		while (av[1][i] == 32 || av[1][i] == 9)
			i++;
		start = i;
		while (av[1][i] > 32 && av[1][i] < 127)
			i++;
		end = i;
		while (av[1][i])
		{
			while (av[1][i] == 32 || av[1][i] == 9)
				i++;
			while (av[1][i] > 32 && av[1][i] < 127) 
			{
				write(1, &av[1][i], 1);
				i++;
			}
			if (av[1][i] == 32 || av[1][i] == 9)
			{
				flag = 1;
				i++;
			}
			if (flag == 1)
				write(1, " ", 1);
		}
		while (start < end)
		{
			write(1, &av[1][start], 1);
			start++;
		}
	}
	write(1, "\n", 1);
}
