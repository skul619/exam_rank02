/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: joleal-b <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/04/03 14:37:38 by joleal-b          #+#    #+#             */
/*   Updated: 2024/04/03 15:01:51 by joleal-b         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>

int	ft_isspace(char c)
{
	if (c == 32 || c == 9 || c == 10)
	{
		return (1);
	}
	return (0);
}

int	ft_count_words(char *str)
{
	int	words;
	int	i;

	words = 0;
	i = 0;
	while (str[i])
	{
		while (str[i] && ft_isspace(str[i]))
			i++;
		if (str[i])
			words++;
		while (str[i] && !ft_isspace(str[i]))
			i++;
	}
	return (words);
}

int	ft_strlen_sep(char *str)
{
	int	i;
	
	i = 0;
	while (str[i] && !ft_isspace(str[i]))
	{
		i++;
	}
	return (i);
}

int	ft_strlcpy(char *dst, char *src, int size)
{
	int	i;
	int	j;

	i = 0;
	j = 0;
	while (src[i])
		i++;
	if (size > 0)
	{
		while (src[j] && (size - 1) > j)
		{
			dst[j] = src[j];
			j++;
		}
		dst[j] = '\0';
	}
	return (i);
}

char	*ft_print_words(char *str)
{
	int	len;
	char	*array;

	len = ft_strlen_sep(str);
	array = (char *)malloc((len + 1) * sizeof(char));
	if (!array)
		return (NULL);
	ft_strlcpy(array, str, len + 1);
	return (array);
}

char	**ft_allowcate(char **array, char *str, int words)
{
	int	i;
	int	j;

	i = 0;
	j = 0;
	while (j < words && str[i])
	{
		while (str[i] && ft_isspace(str[i]))
			i++;
		if (str[i])
		{
			array[j] = ft_print_words(str + i);
			j++;
		}
		while (str[i] && !ft_isspace(str[i]))
			i++;
	}
	array[j] = 0;
	return (array);
}

char	**ft_split(char *str)
{
	char	**array;
	int	words;

	words = ft_count_words(str);
	array = (char **)malloc((words + 1) * sizeof(char *));
	if (!array)
		return (NULL);
	array = ft_allowcate(array, str, words);
	return (array);
}

/*int	main(void)
{
	int	i;
	char	**rest;
	char	*str = "joao matador bbento";

	rest = ft_split(str);
	i = 0;
	while (rest[i])
	{
		puts(rest[i]);
		free(rest[i]);
		i++;
	}
	free(rest);
}*/
