/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rev_wstr2.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: marvin <marvin@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/04/02 13:59:49 by marvin            #+#    #+#             */
/*   Updated: 2024/04/02 13:59:49 by marvin           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void  rev_wstr(char *str)
{
  int i;
  int j;

  i = 0;
  while (str[i])
    i++;
  i--;
  while (i >= 0)
  {
    while (str[i] > 32 && str[i] < 127)
        i--;
    j = i + 1;
    while (str[j] > 32 && str[j] < 127)
    {
      write(1, &str[j], 1);
      j++;
    }
    if (str[i] == 32 || str[i] == 9)
    {
      write(1, " ", 1);
      i--;    
    }
  }
}

int main(int ac, char **av)
{
  if (ac == 2)
      rev_wstr(av[1]);
  write(1, "\n", 1);
}