/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fprime.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: joleal-b <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/28 08:41:59 by joleal-b          #+#    #+#             */
/*   Updated: 2024/03/28 08:54:02 by joleal-b         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>

int main(int ac, char **av)
{
    int n;
    int nbr;

    if(ac == 2)
    {
        nbr = atoi(av[1]);
        if (nbr == 1)
            printf("1");
        n = 2;
        while (nbr >= n)
        {
            if ((nbr % n) == 0)
            {
                printf("%d", n);
                if (nbr == n)
                    break;
                printf("*");
                nbr /= n;
                n = 2;
            }
            ++n;
        }
    }
    printf("\n");
}