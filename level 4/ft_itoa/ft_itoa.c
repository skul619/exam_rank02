/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: joleal-b <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/04/03 10:42:50 by joleal-b          #+#    #+#             */
/*   Updated: 2024/04/03 10:53:02 by joleal-b         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>

int	ft_size_itoa(int n)
{
	long	nbr;
	int	len;

	nbr = n;
	len = 0;
	if (n == 0)
		len++;
	if (n < 0)
	{
		len++;
		n = -n;
	}
	while (n > 0)
	{
		len++;	
		n = n / 10;
	}
	return (len);
}

char	*ft_itoa(int nbr)
{
	long	n;
	int	len;
	char	*str;

	n = nbr;
	len = ft_size_itoa(n);
	str = (char *)malloc((len + 1) * sizeof(char));
	str[len--] = '\0';
	if (n == 0)
	{
		str[0] = '0';
	}
	if (n < 0)
	{
		str[0] = '-';
		n = -n;
	}
	while (n > 0)
	{
		str[len--] = (n % 10) + 48;
		n = n / 10;
	}
	return (str);
}
