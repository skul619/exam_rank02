/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   snake_to_camel.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jpark <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/04/02 15:19:39 by jpark             #+#    #+#             */
/*   Updated: 2024/04/02 15:26:07 by jpark            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_lowercase(char *str)
{
	int	i;

	i = 0;
	while (str[i])
	{
		if (str[i] > 64 && str[i] < 91)
			str[i] = str[i] + 32;
		i++;
	}
}

int	main(int ac, char **av)
{
	int	i;

	i = 0;
	if(ac == 2)
	{
		ft_lowercase(av[1]);
		while (av[1][i])
		{
			if (av[1][i] == 95)
			{
				av[1][i + 1] = av[1][i + 1] - 32;
				i++;
			}
			write(1, &av[1][i], 1);
			i++;
		}
	}
	write(1, "\n", 1);
}
