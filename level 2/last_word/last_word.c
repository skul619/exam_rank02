/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   last_word.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jpark <jpark@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/03/22 17:00:43 by jpark             #+#    #+#             */
/*   Updated: 2024/03/22 18:37:03 by jpark            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void    last_word(char *str)
{
    int i;
    int j;

    i = 0;
    j = 0;
    while (str[i])
    {
        if ((str[i] == 32 || str[i] == 9) 
            && (str[i + 1] > 32 && str[i + 1] < 127))
            j = i + 1;
        i++;
    }
    while (str[j] > 32 && str[j] < 127)
    {
        write(1, &str[j], 1);
        j++;
    }
}

int main(int ac, char **av)
{
    if (ac == 2)
    {
        last_word(av[1]);
    }
    write(1, "\n", 1);
}